class CommunitiesCommunityCategories < ActiveRecord::Migration[5.1]
  def change
  	create_table :communities_community_categories, id: false do |t|
      t.integer :community_id
      t.integer :community_category_id
    end
  end
end

class AddCommunityToCommunityImages < ActiveRecord::Migration[5.1]
  def change
    add_reference :community_images, :community, foreign_key: true
  end
end

class AddButtonToWhyLiveHeres < ActiveRecord::Migration[5.1]
  def change
    add_column :why_live_heres, :image_present, :boolean
    add_column :why_live_heres, :url_link_present, :boolean
  end
end

class AddPublishToCommunityCategories < ActiveRecord::Migration[5.1]
  def change
    add_column :community_categories, :publish, :boolean
  end
end

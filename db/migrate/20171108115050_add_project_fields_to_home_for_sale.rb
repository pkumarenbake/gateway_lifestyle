class AddProjectFieldsToHomeForSale < ActiveRecord::Migration[5.1]
  def change
  	add_column :home_for_sales, :project_name, :string
  	add_column :home_for_sales, :headline, :string
  	add_column :home_for_sales, :description, :text
  end
end

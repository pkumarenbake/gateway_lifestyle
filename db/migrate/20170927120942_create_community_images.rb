class CreateCommunityImages < ActiveRecord::Migration[5.1]
  def change
    create_table :community_images do |t|
      t.string :community_images

      t.timestamps
    end
  end
end

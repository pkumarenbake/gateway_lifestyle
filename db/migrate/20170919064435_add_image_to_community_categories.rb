class AddImageToCommunityCategories < ActiveRecord::Migration[5.1]
  def change
    add_column :community_categories, :image, :string
  end
end

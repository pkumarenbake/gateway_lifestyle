class CreatePdfInfoPacks < ActiveRecord::Migration[5.1]
  def change
    create_table :pdf_info_packs do |t|
      t.string :name
      t.json :info_pdf

      t.timestamps
    end
  end
end

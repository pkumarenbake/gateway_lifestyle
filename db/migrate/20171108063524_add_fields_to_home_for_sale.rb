class AddFieldsToHomeForSale < ActiveRecord::Migration[5.1]
  def change
  	add_column :home_for_sales, :car_space, :integer
    add_column :home_for_sales, :vendor_name, :string
    add_column :home_for_sales, :vendor_email, :string
    add_column :home_for_sales, :vendor_number, :string
    add_column :home_for_sales, :state, :string
    remove_column :home_for_sales, :garage_spaces
    remove_column :home_for_sales, :project_name
    remove_column :home_for_sales, :construction_status
    remove_column :home_for_sales, :street_name
  end
end

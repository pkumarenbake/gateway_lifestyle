class AddTimeZoneToEvent < ActiveRecord::Migration[5.1]
  def change
    add_column :events, :time_zone, :string
  end
end

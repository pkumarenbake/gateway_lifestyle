class AddCommunityToHomeForSale < ActiveRecord::Migration[5.1]
  def change
    add_column :home_for_sales, :community_id, :integer
    add_column :home_for_sales, :home_layout_id, :integer
    add_column :home_for_sales, :featured, :boolean, :default => false
  end
end

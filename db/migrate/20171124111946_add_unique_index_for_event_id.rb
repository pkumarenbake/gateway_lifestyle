class AddUniqueIndexForEventId < ActiveRecord::Migration[5.1]
  def change
  	add_index :events, :event_id, unique: true
  end
end

class AddSeoTitleToCommunities < ActiveRecord::Migration[5.1]
  def change
    add_column :communities, :seo_title, :string
    add_column :communities, :meta_description, :string
  end
end

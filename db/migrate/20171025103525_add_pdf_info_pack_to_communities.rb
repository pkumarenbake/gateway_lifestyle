class AddPdfInfoPackToCommunities < ActiveRecord::Migration[5.1]
  def change
    add_reference :communities, :pdf_info_pack, foreign_key: true
  end
end

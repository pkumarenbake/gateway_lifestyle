class AddFeaturedToPdfInfoPacks < ActiveRecord::Migration[5.1]
  def change
    add_column :pdf_info_packs, :featured, :boolean, :default => false
  end
end

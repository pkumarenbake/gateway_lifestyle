class CreateActivities < ActiveRecord::Migration[5.1]
  def change
    create_table :activities do |t|
      t.string :name
      t.references :community, foreign_key: true
      t.string :distance
      t.string :image

      t.timestamps
    end
  end
end

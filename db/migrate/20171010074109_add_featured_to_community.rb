class AddFeaturedToCommunity < ActiveRecord::Migration[5.1]
  def change
    add_column :communities, :featured, :boolean, :default => false
  end
end

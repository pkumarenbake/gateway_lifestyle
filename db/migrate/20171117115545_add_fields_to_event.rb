class AddFieldsToEvent < ActiveRecord::Migration[5.1]
  def change
    add_column :events, :start_date, :integer
    add_column :events, :start_month, :string
  end
end

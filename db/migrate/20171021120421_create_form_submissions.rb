class CreateFormSubmissions < ActiveRecord::Migration[5.1]
  def change
    create_table :form_submissions do |t|
      t.string :first_name
      t.string :last_name
      t.string :phone
      t.string :email
      t.string :planning
      t.string :additional_info
      t.string :attachment
      t.string :source_url

      t.timestamps
    end
  end
end

class AddFirstNameToCommunities < ActiveRecord::Migration[5.1]
  def change
    add_column :communities, :first_name, :string
    add_column :communities, :last_name, :string
  end
end

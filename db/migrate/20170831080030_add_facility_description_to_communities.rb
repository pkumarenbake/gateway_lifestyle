class AddFacilityDescriptionToCommunities < ActiveRecord::Migration[5.1]
  def change
    add_column :communities, :facility_description, :string
    add_column :communities, :activity_description, :string
    add_column :communities, :modern_facility, :string
    add_column :communities, :salesperson_name, :string
  end
end

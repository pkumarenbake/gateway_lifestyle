class AddCommunityIdToCommunityOffers < ActiveRecord::Migration[5.1]
  def change
    add_reference :community_offers, :community, foreign_key: true
  end
end

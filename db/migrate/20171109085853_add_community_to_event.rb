class AddCommunityToEvent < ActiveRecord::Migration[5.1]
  def change
    add_column :events, :community_id, :integer
  end
end

class AddFeaturedImageToCommunities < ActiveRecord::Migration[5.1]
  def change
    add_column :communities, :featured_image, :string
  end
end

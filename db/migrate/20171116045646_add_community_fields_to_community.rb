class AddCommunityFieldsToCommunity < ActiveRecord::Migration[5.1]
  def change
    add_column :communities, :community_title, :string
    add_column :communities, :community_description, :text
  end
end

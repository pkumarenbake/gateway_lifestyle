class AddHomeForSaleToCommunities < ActiveRecord::Migration[5.1]
  def change
    add_column :communities, :home_for_sale, :integer, :null => false, :default => 0
  end
end

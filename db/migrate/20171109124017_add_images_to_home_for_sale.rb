class AddImagesToHomeForSale < ActiveRecord::Migration[5.1]
  def change
    add_column :home_for_sales, :images, :string,:array=>true
  end
end

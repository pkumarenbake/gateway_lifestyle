class CreateHomeForSales < ActiveRecord::Migration[5.1]
  def change
    create_table :home_for_sales do |t|
      t.string :address
      t.string :listing_name
      t.integer :listing_unique_id
      t.string :record_type
      t.integer :no_of_bedrooms 
      t.integer :no_of_bathrooms
      t.integer :garage_spaces
      t.float :list_price
      t.string :status
      t.string :house_design_type
      t.string :project_name
      t.string :construction_status
      t.string :street_name
      t.timestamps
    end
  end
end

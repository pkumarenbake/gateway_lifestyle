class AddPublishToCommunityOffers < ActiveRecord::Migration[5.1]
  def change
    add_column :community_offers, :publish, :boolean
  end
end

class AddMasterplanImageToCommunities < ActiveRecord::Migration[5.1]
  def change
    add_column :communities, :masterplan_image, :string
    add_column :communities, :legend_image, :string
    add_column :communities, :masterplan_pdf, :json
  end
end

class CreateFacebookLinks < ActiveRecord::Migration[5.1]
  def change
    create_table :facebook_links do |t|
      t.string :link
      t.references :community, foreign_key: true

      t.timestamps
    end
  end
end

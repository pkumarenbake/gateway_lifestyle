class CreateCommunityOffers < ActiveRecord::Migration[5.1]
  def change
    create_table :community_offers do |t|
      t.string :title
      t.text :description
      t.string :image
      t.text :more_about

      t.timestamps
    end
  end
end

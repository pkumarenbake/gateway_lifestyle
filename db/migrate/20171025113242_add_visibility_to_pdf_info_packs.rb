class AddVisibilityToPdfInfoPacks < ActiveRecord::Migration[5.1]
  def change
    add_column :pdf_info_packs, :visibility, :boolean
  end
end

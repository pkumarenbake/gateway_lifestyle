class CreateWhyLiveHeres < ActiveRecord::Migration[5.1]
  def change
    create_table :why_live_heres do |t|
      t.string :title
      t.string :description
      t.string :image
      t.references :community, foreign_key: true
      t.string :youtube_url_link

      t.timestamps
    end
  end
end

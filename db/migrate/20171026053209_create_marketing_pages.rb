class CreateMarketingPages < ActiveRecord::Migration[5.1]
  def change
    create_table :marketing_pages do |t|
      t.string :title
      t.string :slug
      t.string :content
      t.string :meta_tag
      t.string :seo_title
      t.string :meta_description

      t.timestamps
    end
  end
end

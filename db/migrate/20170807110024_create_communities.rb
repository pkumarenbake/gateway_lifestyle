class CreateCommunities < ActiveRecord::Migration[5.1]
  def change
    create_table :communities do |t|
      t.string :short_name, foreign_key: true
      t.string :primary_color
      t.string :secondary_color
      t.string :long_name
      t.references :community_offer, foreign_key: true
      t.boolean :publish 
      t.string :slug
      t.string :community_logo
      t.string :community_icon
      t.json :community_images
      t.string :community_infopack
      t.string :phone
      t.string :email
      t.string :name
      t.string :salesperson_image
      t.string :suburb
      t.string :street_address
      t.string :state
      t.string :post_code
      t.string :full_address
      t.float :longitude
      t.float :latitude
      t.timestamps
    end
  end
end

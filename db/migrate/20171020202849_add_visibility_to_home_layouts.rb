class AddVisibilityToHomeLayouts < ActiveRecord::Migration[5.1]
  def change
    add_column :home_layouts, :visibility, :boolean
  end
end

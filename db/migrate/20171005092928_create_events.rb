class CreateEvents < ActiveRecord::Migration[5.1]
  def change
    create_table :events do |t|
      t.string :name
      t.text :description
      t.datetime :start_time
      t.datetime :end_time
      t.string :category
      t.string :organizer
      t.string :event_id
      t.string :event_url

      t.timestamps
    end
  end
end

class CreateHomeLayouts < ActiveRecord::Migration[5.1]
  def change
    create_table :home_layouts do |t|
      t.string :name
      t.string :floorplan_image
      t.json :floorplan_pdf
      t.string :feature_title1
      t.string :description1
      t.string :feature_title2
      t.string :description2
      t.string :feature_title3
      t.string :description3
      t.string :feature_title4
      t.string :description4

      t.timestamps
    end
  end
end

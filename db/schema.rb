# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20171101065411) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "activities", force: :cascade do |t|
    t.string "name"
    t.bigint "community_id"
    t.string "distance"
    t.string "image"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["community_id"], name: "index_activities_on_community_id"
  end

  create_table "admins", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string "unconfirmed_email"
    t.integer "failed_attempts", default: 0, null: false
    t.string "unlock_token"
    t.datetime "locked_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_admins_on_email", unique: true
    t.index ["reset_password_token"], name: "index_admins_on_reset_password_token", unique: true
  end

  create_table "api_keys", force: :cascade do |t|
    t.string "access_token"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "communities", force: :cascade do |t|
    t.string "short_name"
    t.string "primary_color"
    t.string "secondary_color"
    t.string "long_name"
    t.bigint "community_offer_id"
    t.boolean "publish"
    t.string "slug"
    t.string "community_logo"
    t.string "community_icon"
    t.json "community_images"
    t.string "community_infopack"
    t.string "phone"
    t.string "email"
    t.string "name"
    t.string "salesperson_image"
    t.string "suburb"
    t.string "street_address"
    t.string "state"
    t.string "post_code"
    t.string "full_address"
    t.float "longitude"
    t.float "latitude"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "facility_description"
    t.string "activity_description"
    t.string "modern_facility"
    t.string "salesperson_name"
    t.string "featured_image"
    t.string "masterplan_image"
    t.string "legend_image"
    t.json "masterplan_pdf"
    t.string "home_from"
    t.boolean "featured", default: false
    t.string "seo_title"
    t.string "meta_description"
    t.bigint "pdf_info_pack_id"
    t.string "first_name"
    t.string "last_name"
    t.index ["community_offer_id"], name: "index_communities_on_community_offer_id"
    t.index ["pdf_info_pack_id"], name: "index_communities_on_pdf_info_pack_id"
  end

  create_table "communities_community_categories", id: false, force: :cascade do |t|
    t.integer "community_id"
    t.integer "community_category_id"
  end

  create_table "community_addresses", force: :cascade do |t|
    t.string "full_address"
    t.float "longitude"
    t.float "latitude"
    t.string "suburb"
    t.string "street_address"
    t.bigint "community_id"
    t.string "state"
    t.integer "post_code"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["community_id"], name: "index_community_addresses_on_community_id"
  end

  create_table "community_categories", force: :cascade do |t|
    t.string "title"
    t.string "description"
    t.bigint "community_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "image"
    t.boolean "publish"
    t.index ["community_id"], name: "index_community_categories_on_community_id"
  end

  create_table "community_facilities", force: :cascade do |t|
    t.string "name"
    t.string "image"
    t.bigint "community_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["community_id"], name: "index_community_facilities_on_community_id"
  end

  create_table "community_images", force: :cascade do |t|
    t.string "community_images"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "community_id"
    t.index ["community_id"], name: "index_community_images_on_community_id"
  end

  create_table "community_nearbies", force: :cascade do |t|
    t.string "name"
    t.bigint "community_id"
    t.string "distance"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["community_id"], name: "index_community_nearbies_on_community_id"
  end

  create_table "community_offers", force: :cascade do |t|
    t.string "title"
    t.text "description"
    t.string "image"
    t.text "more_about"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "community_id"
    t.boolean "publish"
    t.index ["community_id"], name: "index_community_offers_on_community_id"
  end

  create_table "contacts", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "events", force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.datetime "start_time"
    t.datetime "end_time"
    t.string "category"
    t.string "organizer"
    t.string "event_id"
    t.string "event_url"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "image_url"
    t.text "venue"
    t.string "address_1"
    t.string "address_2"
    t.string "city"
    t.string "region"
    t.string "postal_code"
    t.string "country"
    t.float "latitude"
    t.float "longitude"
    t.string "localized_address_display"
    t.string "localized_area_display"
    t.string "localized_multi_line_address_display"
  end

  create_table "facebook_links", force: :cascade do |t|
    t.string "link"
    t.bigint "community_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["community_id"], name: "index_facebook_links_on_community_id"
  end

  create_table "form_submissions", force: :cascade do |t|
    t.string "first_name"
    t.string "last_name"
    t.string "phone"
    t.string "email"
    t.string "planning"
    t.string "additional_info"
    t.string "attachment"
    t.string "source_url"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "home_designs", force: :cascade do |t|
    t.string "feature_1"
    t.string "feature_heading_3"
    t.string "feature_heading_2"
    t.string "design_name"
    t.string "feature_heading_1"
    t.string "feature_heading_4"
    t.string "feature_2"
    t.string "feature_4"
    t.string "feature_3"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "home_for_sales", force: :cascade do |t|
    t.string "address"
    t.string "listing_name"
    t.integer "listing_unique_id"
    t.string "record_type"
    t.integer "no_of_bedrooms"
    t.integer "no_of_bathrooms"
    t.integer "garage_spaces"
    t.float "list_price"
    t.string "status"
    t.string "house_design_type"
    t.string "project_name"
    t.string "construction_status"
    t.string "street_name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "home_layouts", force: :cascade do |t|
    t.string "name"
    t.string "floorplan_image"
    t.json "floorplan_pdf"
    t.string "feature_title1"
    t.string "description1"
    t.string "feature_title2"
    t.string "description2"
    t.string "feature_title3"
    t.string "description3"
    t.string "feature_title4"
    t.string "description4"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "visibility"
  end

  create_table "lifestyles", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "marketing_pages", force: :cascade do |t|
    t.string "title"
    t.string "slug"
    t.string "content"
    t.string "meta_tag"
    t.string "seo_title"
    t.string "meta_description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "pdf_info_packs", force: :cascade do |t|
    t.string "name"
    t.json "info_pdf"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "visibility"
    t.boolean "featured", default: false
  end

  create_table "salespeople", force: :cascade do |t|
    t.bigint "admin_id"
    t.string "email", default: "", null: false
    t.string "distance"
    t.string "name"
    t.integer "phone"
    t.bigint "community_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["admin_id"], name: "index_salespeople_on_admin_id"
    t.index ["community_id"], name: "index_salespeople_on_community_id"
  end

  create_table "why_live_heres", force: :cascade do |t|
    t.string "title"
    t.string "description"
    t.string "image"
    t.bigint "community_id"
    t.string "youtube_url_link"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "image_present"
    t.boolean "url_link_present"
    t.index ["community_id"], name: "index_why_live_heres_on_community_id"
  end

  add_foreign_key "activities", "communities"
  add_foreign_key "communities", "community_offers"
  add_foreign_key "communities", "pdf_info_packs"
  add_foreign_key "community_addresses", "communities"
  add_foreign_key "community_categories", "communities"
  add_foreign_key "community_facilities", "communities"
  add_foreign_key "community_images", "communities"
  add_foreign_key "community_nearbies", "communities"
  add_foreign_key "community_offers", "communities"
  add_foreign_key "facebook_links", "communities"
  add_foreign_key "salespeople", "admins"
  add_foreign_key "salespeople", "communities"
  add_foreign_key "why_live_heres", "communities"
end

Rails.application.routes.draw do
  resources :marketing_pages
  resources :events do
  	collection do
      post :update_event
  	end
  end
  resources :communities do
  	collection do
  		post :find_community
      get :find_phone
      get :find_email
      get :find_short_name
      get :dashboard
      post :update_crop
      post :universal_search
      get :featured_community
      get :count_for_home_and_community
  	end
  end
	get 'page_not_found', to: 'communities#page_not_found', as: :page_not_found
	post "communities/new_community" => "communities#new_community"
  resources :community_facilities
  root to: "admin/login#new"	
  
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
 #get 'community_offers#new'
 get 'contacts/index'

 get 'contacts/show'
 #get 'community_offer', to: :new, controller: 'community_offers'
 # resources :contacts
 resources :home_for_sales do
  collection do
    post 'sales_force_callback'
    get :featured_home
    get :sorted_homes
  end
 end
 
 resources :home_layouts
 resources :form_submissions
 resources :community_offers
 resources :community_categories
 resources :pdf_info_packs do
    collection do
      put :featured_pdf_info_pack
      get :featured_pack
    end
  end
namespace :admin do 
 resources :login do
  post 'static_login', on: :collection
  get 'logout', on: :collection
 end
end  

 
end

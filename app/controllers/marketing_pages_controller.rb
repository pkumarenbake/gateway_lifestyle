class MarketingPagesController < ApplicationController
  before_action :set_marketing_page, only: [:show, :edit, :update, :destroy]

  # GET /marketing_pages
  # GET /marketing_pages.json
  def index
    @marketing_pages = MarketingPage.all
  end

  # GET /marketing_pages/1
  # GET /marketing_pages/1.json
  def show
  end

  # GET /marketing_pages/new
  def new
    @marketing_page = MarketingPage.new
  end

  # GET /marketing_pages/1/edit
  def edit
  end

  # POST /marketing_pages
  # POST /marketing_pages.json
  def create
    @marketing_page = MarketingPage.new(marketing_page_params)

    respond_to do |format|
      if @marketing_page.save
        format.html { redirect_to marketing_pages_path, notice: 'Marketing page was successfully created.' }
        format.json { render :show, status: :created, location: @marketing_page }
      else
        format.html { render :new }
        format.json { render json: @marketing_page.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /marketing_pages/1
  # PATCH/PUT /marketing_pages/1.json
  def update
    respond_to do |format|
      if @marketing_page.update(marketing_page_params)
        format.html { redirect_to marketing_pages_path, notice: 'Marketing page was successfully updated.' }
        format.json { render :show, status: :ok, location: @marketing_page }
      else
        format.html { render :edit }
        format.json { render json: @marketing_page.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /marketing_pages/1
  # DELETE /marketing_pages/1.json
  def destroy
    @marketing_page.destroy
    respond_to do |format|
      format.html { redirect_to marketing_pages_url, notice: 'Marketing page was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_marketing_page
      @marketing_page = MarketingPage.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def marketing_page_params
      params.require(:marketing_page).permit(:title, :slug, :content, :meta_tag, :seo_title, :meta_description)
    end
end

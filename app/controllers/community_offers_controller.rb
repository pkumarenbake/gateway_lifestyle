class CommunityOffersController < ApplicationController

	before_action :authenticate_user, unless: :json_request?
	skip_before_action :verify_authenticity_token


	def new
		@community_offer = CommunityOffer.new
	end

	def create
		@community_offer = CommunityOffer.new(community_offer_params)
		if params[:publish_submit]
      @community_offer.publish = true
    else
       @community_offer.publish = false 
	  end
		if @community_offer.save!
		  respond_to do |format|
		  	flash[:info] = 'Offer Created'
	      format.html {redirect_to community_offers_path}
	      format.js
	    end
	  end  
	end


	def show
    @community_offer = CommunityOffer.find(params[:id])
  end

	def index
    @community_offer = CommunityOffer.all
  end


  def edit
    @community_offer = CommunityOffer.find(params[:id])
  end

  def update
	  @community_offer = CommunityOffer.find(params[:id])
	  if @community_offer.update(community_offer_params)
	  	flash[:info] = 'Offer Updated'
	    redirect_to community_offers_path
	  else
	    render 'edit'
	  end
  end

  def destroy
  	@community_offer = CommunityOffer.find_by_id(params[:id])
  	if @community_offer.communities.present?
      flash[:info] = 'Offer can not be deleted. As it is assigned to community'
    else
	    @community_offer.destroy!
	    flash[:info] = 'Offer deleted'
    end
    redirect_to community_offers_path
  end

  def authenticate_user
  	unless session[:user_email].present?
  		redirect_to root_path
  	end
  end 

	private
	  def community_offer_params
	    params.require(:community_offer).permit(:title, :description, :more_about, :image, :publish)
	  end
end

class EventsController < ApplicationController
  require 'rest-client'
  before_action :set_event, only: [:show, :edit, :update, :destroy]
  skip_before_action :verify_authenticity_token, only: [:add_events,:update_event]

  # GET /events
  # GET /events.json
  def index
    @events = Event.all
    @com_events = Event.where.not(community_id: nil).order(:start_time)
  end

  # GET /events/1
  # GET /events/1.json
  def show
    if @event.present?
      if @event.community_id.present?
        @community = Community.where(id: @event.community_id).first
        @offer = CommunityOffer.where(id: @community.community_offer_id).first
        @pdf_info = PdfInfoPack.where(id: @community.pdf_info_pack_id).first
        @events = Event.where(community_id: @community.id).order(:start_time)
        @homes = HomeForSale.where(community_id: @community.id)
        @community_categories = @community.community_categories
        @community_facilities = @community.community_facilities
        @near_bys = @community.activities
        @why_live_heres = @community.why_live_heres
        @facebook_links = @community.facebook_links
        @community_images = @community.community_images
        @events -= [@event]
      else
        @community = ""
        @events = []
        @offer = ''
        @pdf_info = ''
        @homes = []
        @community_categories = []
        @community_facilities = []
        @near_bys = []
        @why_live_heres = []
        @facebook_links = []
        @community_images = []
      end
    end   
  end

  # GET /events/new
  def new
    @event = Event.new
  end

  # GET /events/1/edit
  def edit
  end

  # POST /events
  # POST /events.json
  def create
    @event = Event.new(event_params)

    respond_to do |format|
      if @event.save
        format.html { redirect_to @event, notice: 'Event was successfully created.' }
        format.json { render :show, status: :created, location: @event }
      else
        format.html { render :new }
        format.json { render json: @event.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /events/1
  # PATCH/PUT /events/1.json
  def update
    respond_to do |format|
      if @event.update(event_params)
        format.html { redirect_to @event, notice: 'Event was successfully updated.' }
        format.json { render :show, status: :ok, location: @event }
      else
        format.html { render :edit }
        format.json { render json: @event.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /events/1
  # DELETE /events/1.json
  def destroy
    @event.destroy
    respond_to do |format|
      format.html { redirect_to events_url, notice: 'Event was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def update_event
    @communities = Community.all
    data = RestClient.get(params[:api_url] + "/?token='+token+'&expand=venue",headers={"Authorization": "Bearer FFELGUOVY6SGEOFBCURD"})
    @event = JSON.parse(data, :symbolize_names => true)
    if @event[:status] == "live"
      if Event.where(event_id: @event[:id]).present?
        event = Event.where(event_id: @event[:id]).first
      else
        event = Event.new
      end
      event.lock!
      event.name = @event[:name][:text]
      event.description = @event[:description][:text]
      event.category = @event[:category_id] if @event[:category_id].present?
      event.venue = @event[:venue][:address][:localized_address_display] if @event[:venue].present?
      event.latitude = @event[:venue][:address][:latitude] if @event[:venue].present?
      event.longitude = @event[:venue][:address][:longitude] if @event[:venue].present?
      event.state = @event[:venue][:address][:region] if @event[:venue].present?
      event.organizer = @event[:organizer_id]
      event.start_time = @event[:start][:local].to_time
      event.start_date = event.start_time.try(:strftime, '%d')
      event.start_month = event.start_time.try(:strftime, '%b')
      event.end_time = @event[:end][:local].to_time
      event.time_zone = @event[:start][:timezone]
      event.event_id = @event[:id]
      event.event_url = @event[:url]
      event.image_url = @event[:logo].present? ? @event[:logo][:original][:url] : nil
      @communities.each do |comm|
        if event.name.downcase.include?(comm.short_name.downcase)
          event.community_id = comm.id
          event.state = comm.state
          break;
        end
      end
      event.save!
    elsif @event[:status] == "deleted"
      event = Event.where(event_id: @event[:id]).first
      event.destroy! if event.present?
    end
    render :json => {"status" => "SUCCESS","message" => "Received"}
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_event
      @event = Event.where(:id => params[:id]).first
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def event_params
      params.require(:event).permit(:name, :description, :start_time, :end_time, :category, :organizer, :event_id, :url, :venue, :address_1, :address_2, :city, :region, :postal_code, :country, :latitude, :longitude, :localized_address_display, :localized_area_display, :localized_multi_line_address_display,:state, :time_zone)
    end
end

class PdfInfoPacksController < ApplicationController
	before_action :authenticate_user, unless: :json_request?
	skip_before_action :verify_authenticity_token
	def new
		@pdf_info_packs = PdfInfoPack.new
	end
	def create
		if params[:pdf_info_pack][:featured] == '1'
			@current_fe = PdfInfoPack.where(:featured => true)
			if @current_fe.present?
				@current_fe.update_all(:featured => false)
			end
		else
			@current_fe = PdfInfoPack.where(:featured => true)
			unless @current_fe.present?
				params[:pdf_info_pack][:featured] = '1'
			end
		end
	 @pdf_info_packs = PdfInfoPack.new(pdf_info_pack_params)
	 if params[:visibility_submit]
      @pdf_info_packs.visibility = true 
    else
       @pdf_info_packs.visibility = false 
	  end
	 respond_to do |format|
    if @pdf_info_packs.save
			format.json { redirect_to pdf_info_packs_path, status: :created}
			flash[:info] = 'PdfInfo Created'
			format.html {redirect_to pdf_info_packs_path}
		else
			format.json { render json: pdf_info_packs.errors, status: :unprocessable_entity}
			format.html { render 'new'}
		end
    end
	end

	def index
    @pdf_info_packs = PdfInfoPack.all
  end

	def show
		@pdf_info_pack = PdfInfoPack.find(params[:id])
	end
	def edit
    @pdf_info_packs = PdfInfoPack.find(params[:id])
  end


  def featured_pdf_info_pack

		if params[:id].present? && params[:value].present?
			@current_fe = PdfInfoPack.where(:featured => true)
			if @current_fe.present?
				@current_fe.update_all(:featured => false)
			end
			@pdf_info = PdfInfoPack.where(id: params[:id]).first
			@pdf_info.update_attributes!(:featured => params[:value]) if @pdf_info.present?
		end
		render json: { message: "updated" }, status: :ok
	end


  def update
  	if params[:pdf_info_pack][:featured] == '1'
		@current_fe = PdfInfoPack.where(:featured => true)
		if @current_fe.present?
			@current_fe.update_all(:featured => false)
		end
	else
		@current_fe = PdfInfoPack.where(:featured => true).first
		if @current_fe.present? && @current_fe.id == params[:id].to_i
			params[:pdf_info_pack][:featured] = '1'
		end
	end
	  @pdf_info_pack = PdfInfoPack.find(params[:id])
	  if @pdf_info_pack.update(pdf_info_pack_params)
	  	flash[:info] = 'PdfInfo Updated'
	    redirect_to pdf_info_packs_path
	  else
	    render 'edit'
	  end
  end


	def destroy
    @pdf_info_pack = PdfInfoPack.find(params[:id])
    if @pdf_info_pack.communities.present?
      flash[:info] = 'PdfInfoPack can not be deleted. As it is assigned to community'
    elsif @pdf_info_pack.featured
    	flash[:info] = 'Generic PDF Info Pack cannot be deleted'
    else
	    @pdf_info_pack.destroy!
	    flash[:info] = 'PdfInfoPack deleted'
    end
		  redirect_to pdf_info_packs_path
	end

	

  def authenticate_user
  	unless session[:user_email].present?
  		redirect_to root_path
  	end
  end 

  def featured_pack
  	@info_pack = PdfInfoPack.where(featured: true).first
  	if @info_pack.present?
  	  render :json => {"status" => "SUCCESS","info_pack": @info_pack}
  	else
  		render :json => {"status" => "SUCCESS","message": "Generic Info Pack is not present"}
  	end
  end

	private
	  def pdf_info_pack_params
	    params.require(:pdf_info_pack).permit(:name, :info_pdf, :visibility,:featured)
	  end
end
	
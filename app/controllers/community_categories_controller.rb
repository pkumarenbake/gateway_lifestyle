class CommunityCategoriesController < ApplicationController
	def new
		@community_categories = CommunityCategory.new
	end
	
	def create
	 @community_categories = CommunityCategory.new(community_category_params)
	 if params[:publish_submit]
      @community_categories.publish = true
    else
       @community_categories.publish = false 
	  end
	 respond_to do |format|
    if @community_categories.save
			format.json { redirect_to community_categories_path, status: :created}
			flash[:info] = 'Category Created'
			format.html {redirect_to community_categories_path}
		else
			format.json { render json: community_categories.errors, status: :unprocessable_entity}
			format.html { render 'new'}
		end
    end
	end


	def index
    @community_categories = CommunityCategory.all
  end

	def show
		@community_category = CommunityCategory.find(params[:id])
	end
	def edit
    @community_categories = CommunityCategory.find(params[:id])
  end

  def update
	  @community_category = CommunityCategory.find(params[:id])
	  if @community_category.update(community_category_params)
	  	flash[:info] = 'Category Updated'
	    redirect_to community_categories_path
	  else
	    render 'edit'
	  end
  end



	def destroy
	  @community_category = CommunityCategory.find(params[:id])
	  @community_category.destroy
	 	flash[:info] = 'Category deleted'
	  redirect_to community_categories_path
	end

	private
	  def community_category_params
	    params.require(:community_category).permit(:title, :description, :image, :publish)
	  end
end

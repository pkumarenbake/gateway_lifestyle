class FormSubmissionsController < ApplicationController
	def index
    @form_submissions = FormSubmission.all
  end

  def show
  	@form_submission = FormSubmission.find(params[:id])
    if @form_submission.present?
    	respond_to do |format|
	    	format.html
	      format.json { render json: @form_submission }
	  	end
    else
	  	redirect_to :page_not_found
	  end	
  end

  def new
    @form_submission = FormSubmission.new
  end


  def edit
  	@form_submission = FormSubmission.find(params[:id])
  end

  def create
    @form_submission = FormSubmission.new(form_submission_params)
	  respond_to do |format|
	    if @form_submission.save
				format.json { redirect_to form_submissions_path, status: :created}
				flash[:info] = 'Submission Created'
				format.html {redirect_to form_submissions_path}
			else
				format.json { render json: @form_submission.errors, status: :unprocessable_entity}
				format.html { render 'new'}
			end
	end
  end	

  def update
    @form_submission = FormSubmission.find(params[:id])
	  if @form_submission.update(form_submission_params)
	  	flash[:info] = 'Submission Updated'
	    redirect_to form_submissions_path
	  else
	    render 'edit'
	  end
  end

  def destroy
    @form_submission = FormSubmission.find(params[:id])
	  @form_submission.destroy
	 	flash[:info] = 'Submission deleted'
	  redirect_to form_submissions_path
  end


  private
    def form_submission_params
      params.require(:form_submission).permit(:first_name, :last_name, :phone, :email, :planning, :additional_info, :attachment, :source_url)
    end
end

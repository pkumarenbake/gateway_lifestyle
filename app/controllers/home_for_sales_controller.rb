class HomeForSalesController < ApplicationController
 # include Databasedotcom::Rails::Controller
 skip_before_action :verify_authenticity_token

 def index
 	# @home_for_sale = Listing.all
 	@home_for_sale = HomeForSale.all
 	@comm_home = HomeForSale.where.not(community_id: nil)
 	respond_to do |format|
  	format.html
    format.json { render :json => {"status" => "SUCCESS", "home_for_sale": @comm_home.as_json(include: [:community])} }
	end
 end

 def show
 	@home_for_sale = HomeForSale.where(id: params[:id]).first
 	if @home_for_sale.present?
	 	@home_design = @home_for_sale.home_layout_id.present? ? HomeLayout.where(id: @home_for_sale.home_layout_id).first : ''
		if @home_for_sale.community_id.present?
		    @community = Community.where(id: @home_for_sale.community_id).first
		    @offer = @home_for_sale.record_type == "New" ? CommunityOffer.where(id: @community.community_offer_id).first : ''
		    @pdf_info = PdfInfoPack.where(id: @community.pdf_info_pack_id).first
		    @events = Event.where(community_id: @community.id).order(:start_time)
		    @homes = HomeForSale.where(community_id: @community.id)
		    @community_categories = @community.community_categories
		    @community_facilities = @community.community_facilities
		    @near_bys = @community.activities
		    @why_live_heres = @community.why_live_heres
		    @facebook_links = @community.facebook_links
		    @community_images = @community.community_images
		    @homes -= [@home_for_sale]
	    else
		    @community = ""
		    @events = []
		    @offer = ''
		    @pdf_info = ''
		    @homes = []
		    @community_categories = []
		    @community_facilities = []
		    @near_bys = []
		    @why_live_heres = []
		    @facebook_links = []
		    @community_images = []
	    end
	end   
	respond_to do |format|
 		if @home_for_sale.present?
	  	format.html
	    format.json { render :json => {"status" => "SUCCESS","home_for_sale": @home_for_sale,"home_design": @home_design,"home_community": @community, community_categories: @community_categories, "community_facilities": @community_facilities, "near_bys": @near_bys, "why_live_heres": @why_live_heres,"facebook_links": @facebook_links, "community_images": @community_images, "community_events": @events, "community_offer": @offer, "community_info_pack": @pdf_info, "other_homes": @homes} }
	  else
	  	format.html
	    format.json { render :json => {"status" => "ERROR","message": "Please send correct id"} }
		end
	end
 end

def destroy	
	@home_for_sale =  HomeForSale.find_by_id(params[:id])
	if @home_for_sale.community_id.present?
	  @comm = Community.where(id: @home_for_sale.community_id).first
	  @comm.home_for_sale = @comm.home_for_sale - 1
	  @comm.save!
	end
	@home_for_sale.destroy
	redirect_to home_for_sales_path
end

def sales_force_callback
	data = params.values[0].nil? ? JSON.parse(params.keys[0], :symbolize_names => true) : params
	@communities = Community.all
	@home = HomeForSale.where(listing_unique_id: data[:listing_unique_id]).first
	if @home.present?
		if data[:status] == "withdrawn" || data[:status] == "offmarket"
			@home.destroy!
		else
			@home.listing_name = data[:listing_name]
			if data[:record_type] == "Display Home"
				@home.record_type = "New"
			elsif data[:record_type] == "Preloved Home"
				@home.record_type = "Pre-loved"
			else
			  @home.record_type = data[:record_type]
			end
			@home.no_of_bedrooms = data[:no_of_bedrooms]
			@home.no_of_bathrooms = data[:no_of_bathrooms]
			@home.project_name = data[:project_name]
			@home.headline = data[:headline]
			@home.description = data[:description]
			@home.car_space = data[:carports]
			@home.vendor_name = data[:vendorDetails][:name]
			@home.vendor_number = data[:vendorDetails][:telephone]
			@home.vendor_email = data[:vendorDetails][:email]
			@home.state = data[:address][:state]
			@home.list_price = data[:list_price]
			@home.images = data[:images].as_json.values.map(&:values).flatten if data[:images].present? && data[:images].as_json.try(:values).present?
			@home.status = data[:status]
			@home.house_design_type = data[:house_design_type]
			if data[:address][:lotNumber].present?
				@home.address = data[:address][:lotNumber]+ '/' + data[:address][:streetNumber]+ ', ' + data[:address][:street]
			else
				@home.address = data[:address][:streetNumber]+ ', ' + data[:address][:street]
			end
			@home.address1 = data[:address][:suburb] + ', ' + data[:address][:state] + ', ' + data[:address][:postcode]
			@layout = HomeLayout.where(name: data[:house_design_type]).first
			@home.home_layout_id = @layout.id if @layout.present?
			if @home.community_id.nil?
				@communities.each do |comm|
	        if @home.project_name.downcase.include?(comm.short_name.downcase)
            @home.community_id = comm.id
            comm.home_for_sale = comm.home_for_sale + 1
            comm.save!
	          break;
	        end
	      end
	    end
			@home.save!
			@home.delay.upload_on_cloudinary
		end
	else
		unless data[:status] == "withdrawn" || data[:status] == "offmarket"
			@home = HomeForSale.new
			@home.listing_name = data[:listing_name]
			@home.listing_unique_id = data[:listing_unique_id]
			if data[:record_type] == "Display Home"
				@home.record_type = "New"
			elsif data[:record_type] == "Preloved Home"
				@home.record_type = "Pre-loved"
			else
			  @home.record_type = data[:record_type]
			end
			@home.no_of_bedrooms = data[:no_of_bedrooms]
			@home.no_of_bathrooms = data[:no_of_bathrooms]
			@home.project_name = data[:project_name]
			@home.headline = data[:headline]
			@home.description = data[:description]
			@home.car_space = data[:carports]
			@home.vendor_name = data[:vendorDetails][:name]
			@home.vendor_number = data[:vendorDetails][:telephone]
			@home.vendor_email = data[:vendorDetails][:email]
			@home.state = data[:address][:state]
			@home.list_price = data[:list_price]
			@home.images = data[:images].as_json.values.map(&:values).flatten if data[:images].present? && data[:images].as_json.try(:values).present?
			@home.status = data[:status]
			@home.house_design_type = data[:house_design_type]
			if data[:address][:lotNumber].present?
				@home.address = data[:address][:lotNumber]+ '/' + data[:address][:streetNumber]+ ', ' + data[:address][:street]
			else
				@home.address = data[:address][:streetNumber]+ ', ' + data[:address][:street]
			end
			@home.address1 = data[:address][:suburb] + ', ' + data[:address][:state] + ', ' + data[:address][:postcode]
			@layout = HomeLayout.where(name: data[:house_design_type]).first
			@home.home_layout_id = @layout.id if @layout.present?
			@communities.each do |comm|
        if @home.project_name.downcase.include?(comm.short_name.downcase)
          @home.community_id = comm.id
          comm.home_for_sale = comm.home_for_sale + 1
          comm.save!
          break;
        end
      end
			@home.save!
	    @home.delay.upload_on_cloudinary
		end
	end
  render :json => {"status" => "SUCCESS","message" => "Received"}
end

def edit
	@home_for_sale = HomeForSale.where(id: params[:id]).first
end

def sorted_homes
  if params[:sort].present? && params[:sort] == "DESC"
	  @homes = HomeForSale.where.not(community_id: nil).order(list_price: :desc)
	else
		@homes = HomeForSale.where.not(community_id: nil).order(list_price: :asc)
	end
	render :json => {"status" => "SUCCESS","sorted_homes": @homes.as_json( :include => [:community => {:only => [:short_name,:first_name, :last_name, :community_logo]}] ) }
end

def update
	@home_for_sale = HomeForSale.where(id: params[:id]).first
	@home_for_sale.update_column("featured", params[:home_for_sale][:featured])
	redirect_to home_for_sales_path
end

def featured_home
	@homes = HomeForSale.where(featured: true).where.not(community_id: nil)
	render :json => {"status" => "SUCCESS","homes": @homes.as_json(include: [:community])}
end


 # private
	# def home_for_sale_params
	#   params.require(:home_for_sale).permit(:address,:house_design_type, :status, :no_of_bedrooms, :no_of_bathrooms, :garage_spaces)
	# end

end

class HomeLayoutsController < ApplicationController
	def index
    @home_layouts = HomeLayout.all
  end

  def show
  	@home_layout = HomeLayout.find(params[:id])
    if @home_layout.present?
    	respond_to do |format|
	    	format.html
	      format.json { render json: @home_layout }
	  	end
    else
	  	redirect_to :page_not_found
	  end	
  end

  def new
    @home_layout = HomeLayout.new
  end


  def edit
  	@home_layout = HomeLayout.find(params[:id])
  end

  def create
    @home_layout = HomeLayout.new(home_layout_params)
    if params[:visibility]
      @home_layout.visibility = true
    else
       @home_layout.visibility = false 
	end
	  respond_to do |format|
	    if @home_layout.save
				format.json { redirect_to home_layouts_path, status: :created}
				flash[:info] = 'Layout Created'
				format.html {redirect_to home_layouts_path}
			else
				format.json { render json: @home_layout.errors, status: :unprocessable_entity}
				format.html { render 'new'}
			end
    end
  end

  def update
    @home_layout = HomeLayout.find(params[:id])
	  if @home_layout.update(home_layout_params)
	  	flash[:info] = 'Layout Updated'
	    redirect_to home_layouts_path
	  else
	    render 'edit'
	  end
  end

  def destroy
    @home_layout = HomeLayout.find(params[:id])
	  @home_layout.destroy
	 	flash[:info] = 'Layout deleted'
	  redirect_to home_layouts_path
  end


  private
    def home_layout_params
      params.require(:home_layout).permit(:name, :floorplan_image, :floorplan_pdf, :feature_title1, :description1, :feature_title2, :description2, :feature_title3, :description3, :feature_title4, :description4, :visibility)
    end
end

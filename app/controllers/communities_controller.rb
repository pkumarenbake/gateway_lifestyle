class CommunitiesController < ApplicationController
	before_action :authenticate_user, unless: :json_request?
	skip_before_action :verify_authenticity_token

	def new
		@communities = Community.new
		@communities.facebook_links.build
		@communities.community_images.build
		@community_facilities = @communities.community_facilities
	end

	def index
		if params[:sort_by].present?
		  @communities = params[:sort_by] == "name" ? Community.order(:first_name) : Community.order(:updated_at).reverse
		else  
    	@communities = Community.all
    	@published_communities = Community.where(publish: true)
    end
    respond_to do |format|
    	format.html
      format.json { render :json => {"status" => "SUCCESS","communities": @published_communities} }
  	end
  end


  def update_crop
    @community = Community.find_by_slug(params[:id])
    @community.crop(params[:x].to_i, params[:y].to_i, params[:h].to_i, params[:w].to_i)

    redirect_to(communities_path)
  end

  def show
    @communities = Community.find_by_slug(params[:id])
    if @communities.present?
    	@events = Event.where(community_id: @communities.id).order(:start_time)
    	respond_to do |format|
	    	format.html
	      format.json { render json: @communities }
	  	end
    else
	  	redirect_to :page_not_found
	  end	
  end

	def create
	 @communities = Community.new(community_params)
	 @communities.slug = params[:community][:short_name].downcase
	  if params[:publish_submit]
      @communities.publish = true
	  else
      @communities.publish = false 
	  end
	  respond_to do |format|
	    if @communities.save
				format.json { redirect_to communities_path, status: :created}
				flash[:info] = 'Community Created'
				format.html {redirect_to communities_path}
			else
				format.json { render json: @communities.errors, status: :unprocessable_entity}
				format.html { render 'new'}
			end
    end
	end

	def edit
    @communities = Community.find_by_slug(params[:id])
    @events = Event.where(community_id: @communities.id).order(:start_time) if @communities.present?
  end

  def update
  	@communities = Community.find_by_slug(params[:id])
  	@short_name = @communities.short_name
	  @events = Event.where(community_id: @communities.id).order(:start_time) if @communities.present?
	  @communities.touch
	  if @communities.update(community_params)
	  	if params[:publish_submit]
	      @communities.update_attributes(:publish => true)
		  else
	      @communities.update_attributes(:publish => false)
		  end
	  	flash[:info] = 'Community Updated'
	    redirect_to communities_path
	  else
	  	@communities.short_name = @short_name
	    render 'edit'
	  end
  end
  
  def find_community
  	@community = Community.where("slug = ?", params[:community][:short_name]).first
		respond_to do |format|
			if @community.present?
				@events = Event.where(community_id: @community.id).order(:start_time)
				@offer = CommunityOffer.where(id: @community.community_offer_id).first
				@pdf_info = PdfInfoPack.where(id: @community.pdf_info_pack_id).first
				@homes = HomeForSale.where(community_id: @community.id)
				format.html
	      format.json { render :json => {"status" => "SUCCESS","community": @community, community_categories: @community.community_categories, "community_facilities": @community.community_facilities.order(:created_at), "near_bys": @community.activities.order(:created_at), "why_live_heres": @community.why_live_heres.order(:created_at),"facebook_links": @community.facebook_links, "community_images": @community.community_images, "events": @events, "offer": @offer, "info_pack": @pdf_info, "homes": @homes} }
	    else
	    	format.html
	      format.json { render :json => {"status" => "ERORR","message": "Community Not Found"} }
	    end
		end
  end

  def find_short_name
  	@communities = Community.find_by_short_name(params[:short_name])
  	if @communities.present?
      msg = "Short Name is already present"
    end
		render :json => { short_name_exist: msg}.to_json	
  end

  def authenticate_user
  	unless session[:user_email].present?
  		redirect_to root_path
  	end
  end 
  def destroy
    @communities = Community.find_by_id(params[:id])
    if @communities.present?
    	@events = Event.where(community_id: @communities.id)
    	@homes = HomeForSale.where(community_id: @communities.id)
    	@events.destroy_all if @events.present?
    	@homes.destroy_all if @homes.present?
    end
    @communities.destroy
    flash[:info] = 'Community deleted'
    redirect_to communities_path
  end

	def page_not_found
	  respond_to do |format|
	    format.html { render file: "#{Rails.root}/public/404", layout: false, status: :not_found }
	    format.xml { head :not_found }
	    format.any { head :not_found }
	  end
	end  

	def dashboard
		
	end

	def universal_search
		if params[:search].present?
		  @communities = Community.search(params[:search])
		  @events = Event.search(params[:search]).sort_by(&:start_time)
		  @home_for_sales = HomeForSale.search(params[:search])
		  respond_to do |format|
				format.html
	      format.json { render :json => {"status" => "SUCCESS","community": @communities, events: @events, "home_for_sales": @home_for_sales} }
			end
		else
			respond_to do |format|
				format.html
	      format.json { render :json => {"status" => "ERROR","message": "Search value is not present" } }
			end
		end
	end

	def featured_community
		@communities = Community.where(featured: true,publish: true)
  	render :json => {"status" => "SUCCESS","communities": @communities}
	end

	def count_for_home_and_community
		community_count = Community.count
		home_count = HomeForSale.where.not(community_id: nil).count
		render :json => {"status" => "SUCCESS","community_count": community_count, "home_count": home_count}
	end

	private
	  def community_params
	    params.require(:community).permit(:community_title, :community_description,:home_for_sale,:first_name,:last_name,:short_name,:community_offer_id,:pdf_info_pack_id, :primary_color, :secondary_color, :home_from, :salesperson_name, :modern_facility, :activity_description, :facility_description, :long_name, :publish, :featured, :community_logo, :email, :name, :phone, :salesperson_image, :featured_image, :masterplan_image, :legend_image, :masterplan_pdf, :community_infopack, :community_icon,:suburb,:street_address,:state, :post_code ,:seo_title, :meta_description, :crop_x, :crop_y, :crop_w, :crop_h, community_facilities_attributes: [:id,:name,:image, :_destroy], activities_attributes: [:id,:name,:image,:distance, :_destroy], why_live_heres_attributes: [:id,:title,:description,:image,:image_present,:youtube_url_link, :_destroy], community_images_attributes: [:id,:community_images,:_destroy],facebook_links_attributes: [:id,:link,:_destroy],community_category_ids: [])
	  end
end
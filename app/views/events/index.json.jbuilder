json.status "SUCCESS"
json.events do
  json.array! @com_events, partial: 'events/event', as: :event
end
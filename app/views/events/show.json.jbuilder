if @event.present?
	json.status "SUCCESS"
	json.events_in_community @events
	json.event_community @community
	json.community_offer @offer
	json.community_info_pcak @pdf_info
	json.community_homes @homes
	json.community_categories @community_categories
	json.community_facilitis @community_facilities
	json.community_near_bys @near_bys
	json.community_why_live @why_live_heres
	json.community_fb_links @facebook_links
	json.community_images @community_images
	json.partial! "events/event", event: @event
else
	json.status "ERROR"
	json.message "Please send correct id"
end 	

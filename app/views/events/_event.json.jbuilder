json.extract! event, :id, :name, :description, :state, :time_zone, :start_time, :start_date, :start_month, :end_time, :category, :venue, :latitude, :longitude, :organizer, :event_url, :image_url, :event_id, :created_at, :updated_at
json.url event_url(event, format: :json)

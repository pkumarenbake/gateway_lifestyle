json.extract! marketing_page, :id, :title, :slug, :content, :meta_tag, :seo_title, :meta_description, :created_at, :updated_at
json.url marketing_page_url(marketing_page, format: :json)

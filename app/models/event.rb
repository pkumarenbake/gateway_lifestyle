class Event < ApplicationRecord
  include AlgoliaSearch
  belongs_to :community, optional: true
	algoliasearch do
    # all attributes will be sent
  end
end

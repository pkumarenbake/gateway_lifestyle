class Community < ApplicationRecord
  include AlgoliaSearch

  has_and_belongs_to_many :community_categories,dependent: :destroy
  has_one :pdf_info_pack
  has_many :community_facilities, inverse_of: :community, dependent: :destroy
  has_one :community_offer
  # belongs_to :home_for_sale
  has_many :activities, inverse_of: :community,dependent: :destroy
  has_many :why_live_heres, inverse_of: :community,dependent: :destroy
  has_many :facebook_links, inverse_of: :community,dependent: :destroy
  has_many :community_images, inverse_of: :community, dependent: :destroy
  accepts_nested_attributes_for :community_facilities, reject_if: :all_blank, allow_destroy: true
  accepts_nested_attributes_for :activities, reject_if: :all_blank, allow_destroy: true
  accepts_nested_attributes_for :why_live_heres, reject_if: :all_blank, allow_destroy: true
  accepts_nested_attributes_for :facebook_links, reject_if: :all_blank, allow_destroy: true
  accepts_nested_attributes_for :community_images, reject_if: :all_blank, allow_destroy: true
  validates_presence_of :slug
  validates_presence_of :short_name
  validates_uniqueness_of :short_name, :slug
  # validates :community_icon, presence: true
  # validates :community_logo, presence: true
  # validates :salesperson_image, presence: true
  # validates :featured_image, presence: true
  after_create :update_slug
  before_update :assign_slug
  mount_uploader :community_logo, ImageUploader
  mount_uploader :community_icon, ImageUploader
  mount_uploader :community_infopack, AttachmentUploader
  mount_uploader :salesperson_image, ImageUploader
  mount_uploader :featured_image, ImageUploader   
  mount_uploader :masterplan_pdf, MasterplanUploader
  mount_uploader :masterplan_image, ImageUploader 
  mount_uploader :legend_image, ImageUploader 
  enum state_name: [
    "Queensland", "New South Wales", "Victoria"
  ]

  geocoded_by :street_address, :latitude => :latitude, :longitude => :longitude
  after_validation :geocode

  algoliasearch do
    # all attributes will be sent
  end
  
	def slug
    self.short_name.downcase.gsub(" ", "-")  
  end
  
  def to_param
    slug
  end
  private

  def assign_slug
    self.slug = self.short_name.downcase.gsub(" ", "-")
  end

  def update_slug
    update_attributes slug: assign_slug
  end
  
end

class HomeForSale < ApplicationRecord
	include AlgoliaSearch
  
  belongs_to :community, optional: true
	algoliasearch do
    # all attributes will be sent
  end

  def upload_on_cloudinary
  	new_images = []
  	self.images.each do |img|
  	  url = Cloudinary::Uploader.upload(img)
  	  new_images << url["secure_url"]
  	end
  	self.images = new_images
  	self.save!
  end
end

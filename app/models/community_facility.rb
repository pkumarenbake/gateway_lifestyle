class CommunityFacility < ApplicationRecord
  mount_uploader :image, FacilityImageUploader
  belongs_to :community, inverse_of: :community_facilities
end

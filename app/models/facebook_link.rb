class FacebookLink < ApplicationRecord
  belongs_to :community, inverse_of: :facebook_links
end

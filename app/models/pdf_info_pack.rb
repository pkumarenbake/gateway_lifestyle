class PdfInfoPack < ApplicationRecord
  has_many :communities
  mount_uploader :info_pdf, PdfinfoUploader
  validates_size_of :info_pdf, maximum: 20.megabytes, message: "should be less than 20 MB"
end

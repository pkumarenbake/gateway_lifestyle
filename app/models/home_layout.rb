class HomeLayout < ApplicationRecord
	mount_uploader :floorplan_image, LayoutUploader
	mount_uploader :floorplan_pdf, FloorplanUploader
	validates :feature_title1, presence: true
	validates :feature_title2, presence: true
	validates :feature_title3, presence: true
	validates :feature_title4, presence: true
	validates :description1, presence: true
	validates :description2, presence: true
	validates :description3, presence: true
	validates :description4, presence: true
end

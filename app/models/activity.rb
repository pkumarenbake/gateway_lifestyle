class Activity < ApplicationRecord
  mount_uploader :image, ActivityImageUploader
  belongs_to :community, inverse_of: :activities
end

class WhyLiveHere < ApplicationRecord
  #validates :image, presence: true
  mount_uploader :image, WhyLiveHereImageUploader
  belongs_to :community, inverse_of: :why_live_heres
end

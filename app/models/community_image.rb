class CommunityImage < ApplicationRecord
	mount_uploader :community_images, WhyLiveHereImageUploader
  belongs_to :community, inverse_of: :community_images
  validates :community_images, presence: true
end

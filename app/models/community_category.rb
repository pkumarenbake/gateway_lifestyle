class CommunityCategory < ApplicationRecord
	has_and_belongs_to_many :communities
	mount_uploader :image, ImageUploader
end

namespace :remove_home do
  desc 'Update all home for sale'
  task :remove => :environment do
    @homes = HomeForSale.where("date(updated_at) <= ? and status = 'sold'", Date.today - 2.weeks)
    if @homes.present?
	    @homes.destroy_all
	    Community.all.each do |comm|
			  comm.home_for_sale = HomeForSale.where(community_id: comm.id).count
			  @comm.save!
			end
		end
  end
end
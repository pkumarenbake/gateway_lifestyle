namespace :remove_event do
  desc 'Update all tables'
  task :remove => :environment do
    @events = Event.where("date(events.end_time) < ?",Date.today)
    @events.destroy_all if @events.present?
  end
end